package hu.cubix.sco.logistics.service.exception;

public class DecreaseCategoryNotFoundException extends EntityNotFoundException {

    public DecreaseCategoryNotFoundException(String message) {
        super(message);
    }

    public DecreaseCategoryNotFoundException(int delay) {
        super("Decrease not found this delay: " + delay);
    }
}
