package hu.cubix.sco.logistics.model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class TransportPlan {

    @Id
    @GeneratedValue
    private long id;
    private Double expectedIncome;

    @OneToMany(mappedBy = "transportPlan", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER, targetEntity=Section.class)
    private List<Section> sections = new ArrayList<>();

    public TransportPlan() {
    }

    public TransportPlan(long id, Double expectedIncome, List<Section> sections) {
        this.id = id;
        this.expectedIncome = expectedIncome;
        this.sections = sections;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Double getExpectedIncome() {
        return expectedIncome;
    }

    public void setExpectedIncome(Double expectedIncome) {
        this.expectedIncome = expectedIncome;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void addSection(Section section) {
        sections.add(section);
        section.setTransportPlan(this);
    }

    public void setSections(List<Section> sections) {
        sections.forEach(e->this.addSection(e));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransportPlan that = (TransportPlan) o;
        return id == that.id && Objects.equals(expectedIncome, that.expectedIncome) && Objects.equals(sections, that.sections);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, expectedIncome, sections);
    }

    @Override
    public String toString() {
        return "TransportPlan{" +
                "id=" + id +
                ", expectedIncome=" + expectedIncome +
                ", sections=" + sections +
                '}';
    }
}
