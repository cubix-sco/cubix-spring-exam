package hu.cubix.sco.logistics.service;

import hu.cubix.sco.logistics.model.Milestone;
import hu.cubix.sco.logistics.model.Section;
import hu.cubix.sco.logistics.model.TransportPlan;
import hu.cubix.sco.logistics.repository.SectionRepository;
import hu.cubix.sco.logistics.service.exception.SectionNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SectionService {

    @Autowired
    private SectionRepository sectionRepository;

    public List<Section> getAllSection() {
        return sectionRepository.findAll();
    }

    public Section findById(long id) throws SectionNotFoundException {
        return sectionRepository.findById(id).orElseThrow(() -> new SectionNotFoundException(id));
    }

    @Transactional
    public Section saveSection(Section section) {
        return sectionRepository.save(section);
    }

    @Transactional
    public void deleteSection(long id) {
        sectionRepository.deleteById(id);
    }

    @Transactional
    public void deleteAll() { sectionRepository.deleteAll(); }


    public List<Section> getSectionsByTransportPlanIdAndMilestoneId(
            TransportPlan transportPlan,
            Milestone milestone) throws SectionNotFoundException {

        List<Section> sectionList =  sectionRepository.getSectionsByTransportPlanIdAndMilestoneId(
                transportPlan.getId(),
                milestone.getId());

        if (sectionList.isEmpty()) {
            throw new SectionNotFoundException();
        }
        return sectionList;
    }


    public List<Section> getSectionsFromSectionNumber(long tranportPlanId, int fromSectionNumber) {
        return sectionRepository.getSectionsFromSectionNumber(tranportPlanId, fromSectionNumber);
    }


}
