package hu.cubix.sco.logistics.repository;

import hu.cubix.sco.logistics.model.TransportPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TransportPlanRepository extends JpaRepository<TransportPlan, Long> {
}
