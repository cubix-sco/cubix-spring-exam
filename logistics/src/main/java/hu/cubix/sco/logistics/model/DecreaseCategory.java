package hu.cubix.sco.logistics.model;

public class DecreaseCategory {

    public int  delayLimit;
    public int  decreasePercent;

    public DecreaseCategory() {
    }

    public DecreaseCategory(int delayLimit, int decreasePercent) {
        this.delayLimit = delayLimit;
        this.decreasePercent = decreasePercent;
    }

    public int getDelayLimit() {
        return delayLimit;
    }

    public void setDelayLimit(int delayLimit) {
        this.delayLimit = delayLimit;
    }

    public int getDecreasePercent() {
        return decreasePercent;
    }

    public void setDecreasePercent(int decreasePercent) {
        this.decreasePercent = decreasePercent;
    }
}
