package hu.cubix.sco.logistics.util;

import hu.cubix.sco.logistics.model.DecreaseCategory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class DecreaseCategoryUtil {

    private DecreaseCategoryUtil() {}

    private final Logger LOGGER = LoggerFactory.getLogger(DecreaseCategoryUtil.class);

    public static DecreaseCategory selectCategory(int delay, List<DecreaseCategory> decreaseCategories) {
        DecreaseCategory selectedDecreaseCategory = null;

        for (DecreaseCategory decreaseCategory : decreaseCategories) {

            if (delay >= decreaseCategory.getDelayLimit()) {
                if (selectedDecreaseCategory == null) {
                    selectedDecreaseCategory = decreaseCategory;
                } else if (decreaseCategory.getDelayLimit() > selectedDecreaseCategory.getDelayLimit())  {
                    selectedDecreaseCategory = decreaseCategory;
                }
            }
        }
        return selectedDecreaseCategory;
    }
}
