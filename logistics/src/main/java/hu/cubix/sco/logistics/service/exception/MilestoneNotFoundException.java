package hu.cubix.sco.logistics.service.exception;

public class MilestoneNotFoundException extends EntityNotFoundException {

    public MilestoneNotFoundException(String message) {
        super(message);
    }

    public MilestoneNotFoundException(long addressId) {
        super("Milestone with id " + addressId + " not found");
    }
}
