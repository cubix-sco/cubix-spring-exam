package hu.cubix.sco.logistics.service;

import hu.cubix.sco.logistics.model.Milestone;
import hu.cubix.sco.logistics.model.Section;
import hu.cubix.sco.logistics.model.TransportPlan;
import hu.cubix.sco.logistics.repository.TransportPlanRepository;
import hu.cubix.sco.logistics.service.exception.DecreaseCategoryNotFoundException;
import hu.cubix.sco.logistics.service.exception.EntityNotFoundException;
import hu.cubix.sco.logistics.service.exception.SectionNotFoundException;
import hu.cubix.sco.logistics.service.exception.TransportPlanNotFoundException;
import hu.cubix.sco.logistics.service.param.DelayParamObject;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public abstract class TransportPlanService {

    @Autowired
    private TransportPlanRepository transportPlanRepository;

    @Autowired
    private MilestoneService milestoneService;

    @Autowired
    private SectionService sectionService;

    private final Logger LOGGER = LoggerFactory.getLogger(TransportPlanService.class);

    abstract public int getDecreasePercent(int delay) throws DecreaseCategoryNotFoundException;

    public List<TransportPlan> getAllTransportPlan() {
        return transportPlanRepository.findAll();
    }

    public TransportPlan findById(long id) throws TransportPlanNotFoundException {
        return transportPlanRepository.findById(id).orElseThrow(() -> new TransportPlanNotFoundException(id));
    }

    @Transactional
    public TransportPlan saveTransportPlan(TransportPlan transportPlan) {
        return transportPlanRepository.save(transportPlan);
    }

    @Transactional
    public void deleteTransportPlan(long id) {
        transportPlanRepository.deleteById(id);
    }

    @Transactional
    public void deleteAll() { transportPlanRepository.deleteAll(); }

    @Transactional
    public TransportPlan addDelayVersion1(
            long transportPlanId,
            DelayParamObject delayParamObject)
            throws SectionNotFoundException, EntityNotFoundException {

        TransportPlan transportPlan = findById(transportPlanId);
        int decreasePercent = getDecreasePercent(delayParamObject.getDelay());

        Milestone selectedMilestone = milestoneService.findById(delayParamObject.getMilestoneId());

        Section delayedSession = getSectionsByTransportPlanIdAndMilestoneId(transportPlan, selectedMilestone);

        if (delayedSession.getStartMilestone().equals(selectedMilestone)) {
           addDelay(delayParamObject.getDelay(), delayedSession.getStartMilestone());
        } else {
            List<Section> sections = sectionService.getSectionsFromSectionNumber(transportPlan.getId(), delayedSession.getSectionNumber());
            if (sections.size() > 0) {
                Section nextSection = sections.get(0);
                if (!nextSection.getStartMilestone().equals(delayedSession.getEndMilestone())) {
                    addDelay(delayParamObject.getDelay(), nextSection.getStartMilestone());
                }
            }
        }
        addDelay(delayParamObject.getDelay(), delayedSession.getEndMilestone());

        double decreasedIncome = transportPlan.getExpectedIncome() * ( 1 - ( (double)decreasePercent)/100);

        transportPlan.setExpectedIncome(decreasedIncome);
        transportPlanRepository.save(transportPlan);

        return transportPlan;
    }


    @Transactional
    public TransportPlan addDelayVersion2(
            long transportPlanId,
            DelayParamObject delayParamObject)
            throws SectionNotFoundException, EntityNotFoundException {

        TransportPlan transportPlan = findById(transportPlanId);
        int decreasePercent = getDecreasePercent(delayParamObject.getDelay());
        Milestone selectedMilestone = milestoneService.findById(delayParamObject.getMilestoneId());

        Section delayedSection = getSectionsByTransportPlanIdAndMilestoneId(transportPlan, selectedMilestone);

        List<Section> sections = sectionService.getSectionsFromSectionNumber(transportPlan.getId(), delayedSection.getSectionNumber());

        Milestone milestone;

        if (delayedSection.getStartMilestone().equals(selectedMilestone)) {
            milestone = addDelay(delayParamObject.getDelay(), delayedSection.getStartMilestone());
            delayedSection.setStartMilestone(milestone);
        }
        milestone = addDelay(delayParamObject.getDelay(), delayedSection.getEndMilestone());
        delayedSection.setEndMilestone(milestone);


        for (Section section : sections) {
            addDelay(delayParamObject.getDelay(), milestone,  section);
        }

        double decreasedIncome = transportPlan.getExpectedIncome() * ( 1 - decreasePercent/100);

        transportPlan.setExpectedIncome(decreasedIncome);
        transportPlanRepository.save(transportPlan);

        return transportPlan;
    }


    private Milestone addDelay(int delayInMinute, Milestone beforeMilestone, Section section) {
        if (!section.getStartMilestone().equals(beforeMilestone)) {
             addDelay(delayInMinute, section.getStartMilestone());
        }
        Milestone milestone = addDelay(delayInMinute, section.getEndMilestone());
        return milestone;
    }


    private Milestone addDelay(int delayInMinute, Milestone milestone) {
        LocalDateTime delayedTime = milestone.getPlannedTime().plusMinutes(delayInMinute);
        milestone.setPlannedTime(delayedTime);
        milestoneService.saveMilestone(milestone);
        return milestone;
    }


    private Section getSectionsByTransportPlanIdAndMilestoneId(TransportPlan transportPlan, Milestone milestone) throws SectionNotFoundException {
        List<Section> sectionList = sectionService.getSectionsByTransportPlanIdAndMilestoneId(transportPlan, milestone);

        return sectionList.get(0);
    }

}
