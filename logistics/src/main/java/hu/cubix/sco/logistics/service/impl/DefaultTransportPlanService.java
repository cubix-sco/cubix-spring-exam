package hu.cubix.sco.logistics.service.impl;

import hu.cubix.sco.logistics.service.TransportPlanService;
import hu.cubix.sco.logistics.service.exception.DecreaseCategoryNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class DefaultTransportPlanService extends TransportPlanService {

    @Override
    public int getDecreasePercent(int delay) throws DecreaseCategoryNotFoundException {
        return 0;
    }


}
