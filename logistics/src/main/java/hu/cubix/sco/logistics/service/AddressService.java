package hu.cubix.sco.logistics.service;

import hu.cubix.sco.logistics.model.Address;
import hu.cubix.sco.logistics.model.Milestone;
import hu.cubix.sco.logistics.repository.AddressRepository;
import hu.cubix.sco.logistics.repository.MilestoneRepository;
import hu.cubix.sco.logistics.service.exception.AddressNotFoundException;
import hu.cubix.sco.logistics.service.exception.ConnectedEntityException;
import hu.cubix.sco.logistics.service.search.AddressSearchObject;
import hu.cubix.sco.logistics.service.search.AddressSpecifications;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService {

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private MilestoneRepository milestoneRepository;

    public List<Address> findAll() {
        return addressRepository.findAll();
    }

    public Address findById(long id) throws AddressNotFoundException {
        return addressRepository.findById(id).orElseThrow(() -> new AddressNotFoundException(id));
    }

    @Transactional
    public Address saveAddress(Address address) {
        return addressRepository.save(address);
    }

    @Transactional
    public void deleteAddress(long id) throws ConnectedEntityException {
        List<Milestone> milestones = milestoneRepository.findByAddressId(id);
        if (milestones.isEmpty()) {
            addressRepository.deleteById(id);
        } else {
            throw new ConnectedEntityException();
        }
    }

    @Transactional
    public Address updateAddress(long id, Address address) throws AddressNotFoundException {
        Address addressToUpdate = findById(id);
        address.setId(addressToUpdate.getId());
        return addressRepository.save(address);
    }

    public Page<Address> search(AddressSearchObject addressSearchObject, Pageable pageable) {
        Specification<Address> specification = AddressSpecifications.getAddressSearchSpecification(addressSearchObject);
        return addressRepository.findAll(specification, pageable);
    }

    @Transactional
    public void deleteAll() {
        addressRepository.deleteAll();
    }

}
