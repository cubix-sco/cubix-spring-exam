package hu.cubix.sco.logistics.controller;

import hu.cubix.sco.logistics.dto.AddressDto;
import hu.cubix.sco.logistics.mapper.AddressMapper;
import hu.cubix.sco.logistics.model.Address;
import hu.cubix.sco.logistics.service.AddressService;
import hu.cubix.sco.logistics.service.exception.AddressNotFoundException;
import hu.cubix.sco.logistics.service.exception.ConnectedEntityException;
import hu.cubix.sco.logistics.service.search.AddressSearchObject;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.function.Function;

@RestController
@RequestMapping("/api/addresses")
public class AddressRestController {

    @Autowired
    AddressService addressService;

    @Autowired
    AddressMapper addressMapper;

    @GetMapping
    public List<AddressDto> getAllAddresses() {
        return addressMapper.addressesToDtos(addressService.findAll());
    }

    @GetMapping("/{id}")
    public AddressDto findById(@PathVariable long id) {
        try {
            return addressMapper.addressToDto(addressService.findById(id));
        } catch (AddressNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public AddressDto addAddreass(@RequestBody @Valid AddressDto addressDto) {
        if (addressDto == null || addressDto.getId() > 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        Address address = addressMapper.dtoToAddress(addressDto);
        return addressMapper.addressToDto(addressService.saveAddress(address));
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable long id) {
        try {
            addressService.deleteAddress(id);
        } catch (ConnectedEntityException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }


    @PutMapping("/{id}")
    public AddressDto updateById(@PathVariable long id, @RequestBody @Valid AddressDto addressDto) {
        if (addressDto == null || addressDto.getId() > 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        try {
            Address address = addressMapper.dtoToAddress(addressDto);
            return addressMapper.addressToDto(addressService.updateAddress(id, address));
        } catch (AddressNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/search")
    public ResponseEntity<Page<AddressDto>> search(@RequestBody @Valid AddressSearchObject addressSearchObject, Pageable pageable) {
        if (addressSearchObject == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        Page<Address> result = addressService.search(addressSearchObject, pageable);

        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Total-Count", String.valueOf(result.getTotalElements()));

        Page<AddressDto> dtoPages = result.map(new Function<Address, AddressDto>() {
            @Override
            public AddressDto apply(Address entity) {
                return addressMapper.addressToDto(entity);
            }
        });

        return ResponseEntity.ok()
                .headers(headers)
                .body(dtoPages);
    }


}
