package hu.cubix.sco.logistics.controller;

import hu.cubix.sco.logistics.dto.LoginDto;
import hu.cubix.sco.logistics.service.security.JWTService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/login")
public class JWTLoginController {

    @Autowired
    AuthenticationManager authnManager;

    @Autowired
    JWTService jwtService;

    @PostMapping
    public String login(@RequestBody LoginDto loginDto) {
        Authentication authentication = authnManager.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
        return jwtService.createJwt((UserDetails) authentication.getPrincipal());
    }

}
