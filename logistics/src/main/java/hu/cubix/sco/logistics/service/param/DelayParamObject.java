package hu.cubix.sco.logistics.service.param;

public class DelayParamObject {

    int delay;
    long milestoneId;

    public DelayParamObject() {
    }

    public DelayParamObject(int delay, long milestoneId) {
        this.delay = delay;
        this.milestoneId = milestoneId;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public long getMilestoneId() {
        return milestoneId;
    }

    public void setMilestoneId(long milestoneId) {
        this.milestoneId = milestoneId;
    }
}
