package hu.cubix.sco.logistics.service.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PwdEncoderUtil {
    private static BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();


    public static String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }


}
