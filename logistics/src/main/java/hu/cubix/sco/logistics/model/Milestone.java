package hu.cubix.sco.logistics.model;

import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.Objects;

@Entity
public class Milestone {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    private Address address;

    private LocalDateTime plannedTime;

    public Milestone() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public LocalDateTime getPlannedTime() {
        return plannedTime;
    }

    public void setPlannedTime(LocalDateTime plannedTime) {
        this.plannedTime = plannedTime;
    }

    public void addDelay(int delayInMinute) {
        this.plannedTime = this.plannedTime.plusMinutes(delayInMinute);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Milestone milestone = (Milestone) o;
        return id == milestone.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
