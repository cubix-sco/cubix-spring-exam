package hu.cubix.sco.logistics.mapper;

import hu.cubix.sco.logistics.dto.AddressDto;
import hu.cubix.sco.logistics.model.Address;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.jmx.export.annotation.ManagedOperation;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AddressMapper {

    AddressDto addressToDto(Address address);

    Address dtoToAddress(AddressDto addressDto);

    List<AddressDto> addressesToDtos(List<Address> addresses);

    List<Address> dtosToAddresses(List<AddressDto> addressDtos);
}
