package hu.cubix.sco.logistics.service;

import hu.cubix.sco.logistics.model.Milestone;
import hu.cubix.sco.logistics.repository.MilestoneRepository;
import hu.cubix.sco.logistics.service.exception.MilestoneNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MilestoneService {

    @Autowired
    private MilestoneRepository milestoneRepository;

    public List<Milestone> getAllMilestones() {
        return milestoneRepository.findAll();
    }

    public Milestone findById(long id) throws MilestoneNotFoundException {
        return milestoneRepository.findById(id).orElseThrow(() -> new MilestoneNotFoundException(id));
    }

    @Transactional
    public Milestone saveMilestone(Milestone milestone) {
        return milestoneRepository.save(milestone);
    }

    @Transactional
    public void deleteMilestone(long id) {
        milestoneRepository.deleteById(id);
    }

    @Transactional
    public void deleteAll() { milestoneRepository.deleteAll(); }

    @Transactional
    public Milestone addDelay(long milestoneId, int delayInMinute) throws MilestoneNotFoundException {
        Milestone milestone = findById(milestoneId);
        milestone.addDelay(delayInMinute);
        milestoneRepository.save(milestone);
        return milestone;
    }


}
