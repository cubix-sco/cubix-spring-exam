package hu.cubix.sco.logistics.dto;

public class SectionDto {

    private long id;

    private Integer sectionNumber;
    private MilestoneDto startMilestone;
    private MilestoneDto endMilestone;

    public SectionDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getSectionNumber() {
        return sectionNumber;
    }

    public void setSectionNumber(Integer sectionNumber) {
        this.sectionNumber = sectionNumber;
    }

    public MilestoneDto getStartMilestone() {
        return startMilestone;
    }

    public void setStartMilestone(MilestoneDto startMilestone) {
        this.startMilestone = startMilestone;
    }

    public MilestoneDto getEndMilestone() {
        return endMilestone;
    }

    public void setEndMilestone(MilestoneDto endMilestone) {
        this.endMilestone = endMilestone;
    }
}
