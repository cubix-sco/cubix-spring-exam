package hu.cubix.sco.logistics.service.search;

public class AddressSearchObject {

    private String countryISOCode;

    private String city;

    private String street;

    private String zipCode;

    public AddressSearchObject() {
    }

    public AddressSearchObject(String countryISOCode, String city, String street, String zipCode) {
        this.countryISOCode = countryISOCode;
        this.city = city;
        this.street = street;
        this.zipCode = zipCode;
    }

    public String getCountryISOCode() {
        return countryISOCode;
    }

    public void setCountryISOCode(String countryISOCode) {
        this.countryISOCode = countryISOCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
