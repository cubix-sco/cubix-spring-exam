package hu.cubix.sco.logistics.repository;

import hu.cubix.sco.logistics.model.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SectionRepository extends JpaRepository<Section, Long> {


    @Query(value = "SELECT s FROM Section s  WHERE s.transportPlan.id = :tranportPlanId AND (s.startMilestone.id = :milestoneid OR s.endMilestone.id = :milestoneid) ORDER BY s.sectionNumber ASC")
    public List<Section> getSectionsByTransportPlanIdAndMilestoneId(long tranportPlanId, long milestoneid);

    @Query(value = "SELECT s FROM Section s  WHERE s.transportPlan.id = :tranportPlanId AND s.sectionNumber > :fromSectionNumber ORDER BY s.sectionNumber ASC")
    public List<Section> getSectionsFromSectionNumber(long tranportPlanId, int fromSectionNumber);

}
