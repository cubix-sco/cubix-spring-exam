package hu.cubix.sco.logistics.config;

import hu.cubix.sco.logistics.service.TransportPlanService;
import hu.cubix.sco.logistics.service.impl.ProdTransportPlanService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("test")
public class TestConfiguration {

    @Bean
    TransportPlanService transportPlanService() {return new ProdTransportPlanService(); }
}
