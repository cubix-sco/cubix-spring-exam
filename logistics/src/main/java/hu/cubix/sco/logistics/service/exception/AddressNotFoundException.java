package hu.cubix.sco.logistics.service.exception;

public class AddressNotFoundException extends EntityNotFoundException {

    public AddressNotFoundException(String message) {
        super(message);
    }

    public AddressNotFoundException(long addressId) {
        super("Address with id " + addressId + " not found");
    }
}
