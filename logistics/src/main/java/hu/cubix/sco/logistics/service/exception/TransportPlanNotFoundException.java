package hu.cubix.sco.logistics.service.exception;

public class TransportPlanNotFoundException extends EntityNotFoundException {

    public TransportPlanNotFoundException(String message) {
        super(message);
    }

    public TransportPlanNotFoundException(long transportPlanId) {
        super("TransportPlan with id " + transportPlanId + " not found");
    }
}
