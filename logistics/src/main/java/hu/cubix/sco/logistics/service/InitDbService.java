package hu.cubix.sco.logistics.service;

import hu.cubix.sco.logistics.model.Address;
import hu.cubix.sco.logistics.model.Milestone;
import hu.cubix.sco.logistics.model.Section;
import hu.cubix.sco.logistics.model.TransportPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class InitDbService {

    @Autowired
    AddressService addressService;

    @Autowired
    TransportPlanService transportPlanService;

    @Autowired
    MilestoneService milestoneService;

    @Autowired
    SectionService sectionService;


    public void clearDB() {
        transportPlanService.deleteAll();
        sectionService.deleteAll();
        milestoneService.deleteAll();;
        addressService.deleteAll();
    }

    public void insertTestData() {
        insertTestData(false);
    }

    public void insertTestData(boolean repeatMilestones) {
        List<Address> adresses = addAddresses();

        List<Milestone> milestones = addMilestone(adresses);
        List<Section> sections = addSections(milestones, repeatMilestones);
        addTransportPlan(sections);

        Collections.reverse(adresses);
        milestones = addMilestone(adresses);
        sections = addSections(milestones, repeatMilestones);
        addTransportPlan(sections);
    }

    public TransportPlan addTransportPlan(List<Section> sections) {
        TransportPlan transportPlan = new TransportPlan();
        transportPlan.setSections(sections);
        transportPlan.setExpectedIncome(Double.valueOf(1000).doubleValue());
        transportPlanService.saveTransportPlan(transportPlan);
        return transportPlan;
    }


    public List<Section> addSections(List<Milestone> milestones, boolean repeatMilestones) {
        List<Section> sections = new ArrayList<>();
        int n = 2;
        if (repeatMilestones) {
            n = 1;
        }

        for (int i = 0; i < milestones.size() - 2; i=i+n) {
            Milestone startMilestone = milestones.get(i);
            Milestone endMilestone = milestones.get(i+1);
            Section section = new Section();
            section.setStartMilestone(startMilestone);
            section.setEndMilestone(endMilestone);
            section.setSectionNumber(i);
            sections.add(section);
        }
        return sections;
    }


    public List<Milestone> addMilestone(List<Address> addresses) {
        List<Milestone> milestones = new ArrayList<>();
        LocalDateTime plannedTime = LocalDateTime.now();

        for (Address address : addresses) {
            plannedTime = plannedTime.plusDays(1);
            Milestone milestone = new Milestone();
            milestone.setAddress(address);
            milestone.setPlannedTime(plannedTime);
            milestoneService.saveMilestone(milestone);
            milestones.add(milestone);
        }
        return milestones;
    }


    public List<Address> addAddresses() {
        List<Address> addresses = new ArrayList<>();

        Address address = new Address("HU", "Budapest", "Chasar Andras", "1146", "2", 47.5050016, 19.0919339);
        addressService.saveAddress(address);
        addresses.add(address);

        address = new Address("HU", "Esztergom", "Simon Janos", "2500", "2", 47.7892834, 18.7420816);
        addressService.saveAddress(address);
        addresses.add(address);

        address = new Address("SK", "Pozsony", "Mlynské", "1484", "68", 48.1440249, 17.1544583);
        addressService.saveAddress(address);
        addresses.add(address);

        address = new Address("AT", "Wien", "Arsenal", "1030", "1", 48.1881023, 16.390002);
        addressService.saveAddress(address);
        addresses.add(address);

        address = new Address("AT", "Salzburg", "Auerspergstrase", "5020", "61", 47.8079225, 13.0492563);
        addressService.saveAddress(address);
        addresses.add(address);

        address = new Address("DE", "Stuttgart", "Maliweg", "70192", "17", 48.7980584, 9.1674287);
        addressService.saveAddress(address);
        addresses.add(address);

        address = new Address("FR", "Nancy", "Raymond", "70192", "43", 48.68840, 6.1681329);
        addressService.saveAddress(address);
        addresses.add(address);

        address = new Address("FR", "Paris", "Furstemberg", "75006", "6", 48.8550392, 2.324821);
        addressService.saveAddress(address);
        addresses.add(address);

        address = new Address("FR", "Calais", "Vie", "62100", "6", 50.9478612, 1.8544462);
        addressService.saveAddress(address);
        addresses.add(address);

        address = new Address("GB", "Canterbury", "High", "CT1 2DY", "3", 51.2781239, 1.0812912);
        addressService.saveAddress(address);
        addresses.add(address);

        address = new Address("GB", "London", "Portobello", "W11 3DG", "6", 51.5119155, -0.2095676);
        addressService.saveAddress(address);
        addresses.add(address);


        return addresses;
    }


}
