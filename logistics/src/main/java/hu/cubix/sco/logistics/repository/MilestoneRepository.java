package hu.cubix.sco.logistics.repository;

import hu.cubix.sco.logistics.model.Milestone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MilestoneRepository extends JpaRepository<Milestone, Long> {

    List<Milestone> findByAddressId(long addressId);
}
