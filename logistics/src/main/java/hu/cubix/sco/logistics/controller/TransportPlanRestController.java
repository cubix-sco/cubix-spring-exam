package hu.cubix.sco.logistics.controller;

import hu.cubix.sco.logistics.mapper.TransportPlanMapper;
import hu.cubix.sco.logistics.service.TransportPlanService;
import hu.cubix.sco.logistics.service.exception.EntityNotFoundException;
import hu.cubix.sco.logistics.service.exception.SectionNotFoundException;
import hu.cubix.sco.logistics.service.param.DelayParamObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


@RestController
@RequestMapping("/api/transportPlans/")
public class TransportPlanRestController {

    @Autowired
    TransportPlanService transportPlanService;

    @Autowired
    TransportPlanMapper transportPlanMapper;

    private final Logger LOGGER = LoggerFactory.getLogger(TransportPlanRestController.class);

    @PostMapping("/{id}/delay")
    public void addDelay(@PathVariable long id, @RequestBody DelayParamObject delayParamObject) {
        if (delayParamObject != null && delayParamObject.getDelay() > 0) {
            try {
                transportPlanService.addDelayVersion1(id, delayParamObject);

            } catch (SectionNotFoundException e) {
                LOGGER.error(e.getMessage(), e);
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            } catch (EntityNotFoundException e) {
                LOGGER.error(e.getMessage(), e);
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }
        } else {
            LOGGER.error("delayParamObject "+delayParamObject.getDelay());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

}
