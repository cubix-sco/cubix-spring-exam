package hu.cubix.sco.logistics.service.search;

import hu.cubix.sco.logistics.model.Address;
import hu.cubix.sco.logistics.model.Address_;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

public class AddressSpecifications {

    public static Specification<Address> getAddressSearchSpecification(AddressSearchObject addressSearchObject) {
        Specification<Address> specification = Specification.where(null);

        specification = AddressSpecifications.addCitySpecification(specification, addressSearchObject);
        specification = AddressSpecifications.addStreetSpecification(specification, addressSearchObject);
        specification = AddressSpecifications.addCountrySpecification(specification, addressSearchObject);
        specification = AddressSpecifications.addZipCodeSpecification(specification, addressSearchObject);

        return specification;
    }

    public static Specification<Address> addCountrySpecification(
            Specification<Address> specification,
            AddressSearchObject addressSearchObject) {

        if (addressSearchObject.getCountryISOCode() != null && StringUtils.hasText(addressSearchObject.getCountryISOCode())) {
            specification = specification.and(country(addressSearchObject.getCountryISOCode()));
        }
        return specification;
    }


    public static Specification<Address> addCitySpecification(
            Specification<Address> specification,
            AddressSearchObject addressSearchObject) {

        if (addressSearchObject.getCity() != null && StringUtils.hasText(addressSearchObject.getCity())) {
            specification = specification.and(cityName(addressSearchObject.getCity()));
        }
        return specification;
    }

    public static Specification<Address> addStreetSpecification(
            Specification<Address> specification,
            AddressSearchObject addressSearchObject) {

        if (addressSearchObject.getStreet() != null && StringUtils.hasText(addressSearchObject.getStreet())) {
            specification = specification.and(streetName(addressSearchObject.getStreet()));
        }
        return specification;
    }

    public static Specification<Address> addZipCodeSpecification(
            Specification<Address> specification,
            AddressSearchObject addressSearchObject) {

        if (addressSearchObject.getZipCode() != null && StringUtils.hasText(addressSearchObject.getZipCode())) {
            specification = specification.and(zipCode(addressSearchObject.getZipCode()));
        }
        return specification;
    }


    public static Specification<Address> country(String countryISOCode) {
        return (root, cq, cb) -> cb.equal(root.get(Address_.countryISOCode), countryISOCode);
    }

    public static Specification<Address> cityName(String cityName) {
        return (root, cq, cb) -> cb.like(cb.lower(root.get(Address_.city)), cityName.toLowerCase() + "%");
    }

    public static Specification<Address> streetName(String streetName) {
        return (root, cq, cb) -> cb.like(cb.lower(root.get(Address_.street)), streetName.toLowerCase() + "%");
    }

    public static Specification<Address> zipCode(String zipCode) {
        return (root, cq, cb) -> cb.equal(root.get(Address_.zipCode), zipCode);
    }

}
