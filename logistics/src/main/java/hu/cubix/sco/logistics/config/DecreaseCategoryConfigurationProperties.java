package hu.cubix.sco.logistics.config;

import hu.cubix.sco.logistics.model.DecreaseCategory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@ConfigurationProperties(prefix = "logistics")
@Component
public class DecreaseCategoryConfigurationProperties {

    List<DecreaseCategory> decreaseCategories = new ArrayList<>();

    {
        //Default values
        decreaseCategories.add(new  DecreaseCategory( 30, 5));
        decreaseCategories.add(new  DecreaseCategory( 60, 10));
        decreaseCategories.add(new  DecreaseCategory( 120, 20));
    }

    public DecreaseCategoryConfigurationProperties() {
    }

    public DecreaseCategoryConfigurationProperties(List<DecreaseCategory> decreaseCategories) {
        this.decreaseCategories = decreaseCategories;
    }

    public List<DecreaseCategory> getDecreaseCategories() {
        return decreaseCategories;
    }

    public void setDecreaseCategories(List<DecreaseCategory> decreaseCategories) {
        this.decreaseCategories = decreaseCategories;
    }
}
