package hu.cubix.sco.logistics.config;

import hu.cubix.sco.logistics.service.TransportPlanService;
import hu.cubix.sco.logistics.service.impl.DefaultTransportPlanService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("dev")
public class DefaultConfiguration {

    @Bean
    TransportPlanService transportPlanService() {return new DefaultTransportPlanService(); }
}
