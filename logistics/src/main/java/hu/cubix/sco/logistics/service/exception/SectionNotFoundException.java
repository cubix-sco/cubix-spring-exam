package hu.cubix.sco.logistics.service.exception;

public class SectionNotFoundException extends EntityNotFoundException {

    public SectionNotFoundException() {
        super();
    }

    public SectionNotFoundException(String message) {
        super(message);
    }

    public SectionNotFoundException(long sectionId) {
        super("Section with id " + sectionId + " not found");
    }
}
