package hu.cubix.sco.logistics.service.impl;

import hu.cubix.sco.logistics.config.DecreaseCategoryConfigurationProperties;
import hu.cubix.sco.logistics.model.DecreaseCategory;
import hu.cubix.sco.logistics.service.TransportPlanService;
import hu.cubix.sco.logistics.service.exception.DecreaseCategoryNotFoundException;
import hu.cubix.sco.logistics.util.DecreaseCategoryUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ProdTransportPlanService extends TransportPlanService {

    @Autowired
    private DecreaseCategoryConfigurationProperties decreaseConfig;

    @Override
    public int getDecreasePercent(int delay) throws DecreaseCategoryNotFoundException {
        if (delay != 0) {
            DecreaseCategory decreaseCategory = selectCategory(delay);
            if (decreaseCategory == null) {
                String message = "Decrease category not found! Delay: " + delay;
                throw new DecreaseCategoryNotFoundException(message);
            }
            return decreaseCategory.getDecreasePercent();
        } else {
            return 0;
        }
    }

    protected DecreaseCategory selectCategory(int delay) {
        return DecreaseCategoryUtil.selectCategory(delay, decreaseConfig.getDecreaseCategories());
    }
}
