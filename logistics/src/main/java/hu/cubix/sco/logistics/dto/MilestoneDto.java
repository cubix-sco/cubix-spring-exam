package hu.cubix.sco.logistics.dto;

import java.time.LocalDateTime;

public class MilestoneDto {

    private long id;
    private AddressDto address;
    private LocalDateTime plannedTime;

    public MilestoneDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public LocalDateTime getPlannedTime() {
        return plannedTime;
    }

    public void setPlannedTime(LocalDateTime plannedTime) {
        this.plannedTime = plannedTime;
    }
}
