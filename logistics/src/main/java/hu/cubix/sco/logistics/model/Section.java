package hu.cubix.sco.logistics.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.util.Objects;

@Entity
public class Section {

    @Id
    @GeneratedValue
    private long id;

    private Integer sectionNumber;

    @OneToOne
    private Milestone startMilestone;

    @OneToOne
    private Milestone endMilestone;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "TRANSPORT_PLAN_ID", referencedColumnName = "ID")
    private TransportPlan transportPlan;

    public Section() {
    }

    public Section(long id, Integer sectionNumber, Milestone startMilestone, Milestone endMilestone, TransportPlan transportPlan) {
        this.id = id;
        this.sectionNumber = sectionNumber;
        this.startMilestone = startMilestone;
        this.endMilestone = endMilestone;
        this.transportPlan = transportPlan;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getSectionNumber() {
        return sectionNumber;
    }

    public void setSectionNumber(Integer sectionNumber) {
        this.sectionNumber = sectionNumber;
    }

    public Milestone getStartMilestone() {
        return startMilestone;
    }

    public void setStartMilestone(Milestone startMilestone) {
        this.startMilestone = startMilestone;
    }

    public Milestone getEndMilestone() {
        return endMilestone;
    }

    public void setEndMilestone(Milestone endMilestone) {
        this.endMilestone = endMilestone;
    }

    public TransportPlan getTransportPlan() {
        return transportPlan;
    }

    public void setTransportPlan(TransportPlan transportPlan) {
        this.transportPlan = transportPlan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Section section = (Section) o;
        return id == section.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sectionNumber, startMilestone, endMilestone, transportPlan);
    }

    @Override
    public String toString() {
        return "Section{" +
                "id=" + id +
                ", sectionNumber=" + sectionNumber +
                ", startMilestone=" + startMilestone +
                ", endMilestone=" + endMilestone +
                ", transportPlan=" + transportPlan +
                '}';
    }
}
