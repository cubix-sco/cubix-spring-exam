package hu.cubix.sco.logistics.service.exception;

public class ConnectedEntityException extends Exception {

    public ConnectedEntityException() {
    }

    public ConnectedEntityException(String message) {
        super(message);
    }

    public ConnectedEntityException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConnectedEntityException(Throwable cause) {
        super(cause);
    }

    public ConnectedEntityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
