package hu.cubix.sco.logistics.dto;

import java.util.List;

public class TransportPlanDto {

    private long id;
    private Double expectedIncome;
    List<SectionDto> sections;


    public TransportPlanDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Double getExpectedIncome() {
        return expectedIncome;
    }

    public void setExpectedIncome(Double expectedIncome) {
        this.expectedIncome = expectedIncome;
    }

    public List<SectionDto> getSections() {
        return sections;
    }

    public void setSections(List<SectionDto> sections) {
        this.sections = sections;
    }
}
