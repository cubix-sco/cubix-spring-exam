package hu.cubix.sco.logistics.controller;

import hu.cubix.sco.logistics.config.DecreaseCategoryConfigurationProperties;
import hu.cubix.sco.logistics.dto.TransportPlanDto;
import hu.cubix.sco.logistics.model.DecreaseCategory;
import hu.cubix.sco.logistics.model.Section;
import hu.cubix.sco.logistics.model.TransportPlan;
import hu.cubix.sco.logistics.repository.TransportPlanRepository;
import hu.cubix.sco.logistics.service.InitDbService;
import hu.cubix.sco.logistics.service.TransportPlanService;
import hu.cubix.sco.logistics.service.exception.TransportPlanNotFoundException;
import hu.cubix.sco.logistics.service.param.DelayParamObject;
import hu.cubix.sco.logistics.util.DecreaseCategoryUtil;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;
import static  org.assertj.core.api.Assertions.*;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@AutoConfigureTestDatabase
public class TransportPlansIntegrationTest {

    @Autowired
    InitDbService initDbService;

    @Autowired
    WebTestClient webTestClient;

    @Autowired
    TransportPlanRepository transportPlanRepository;

    @Autowired
    private DecreaseCategoryConfigurationProperties decreaseConfig;

    public static final String API_TRANSPORT_PLANS = "/api/transportPlans";

    private final Logger LOGGER = LoggerFactory.getLogger(TransportPlansIntegrationTest.class);

    private final static String PWD = "test";
    private final static String USER = "TransportManager";


    @Qualifier("transportPlanService")
    @Autowired
    private TransportPlanService transportPlanService;

    @BeforeEach
    public  void beforeMethod() {
        initDbService.clearDB();
    }

    @Test
    public void addDelayExpectBadRequest() {
        initDbService.insertTestData();
        TransportPlan transportPlan = getTransportPlan();

        Section firstSection = transportPlan.getSections().get(0);
        DelayParamObject delayParamObject = new DelayParamObject(0, firstSection.getStartMilestone().getId());

        addDelayExpectBadRequest(transportPlan.getId(), delayParamObject, USER, PWD);

        List<TransportPlan> transportPlanList = transportPlanRepository.findAll();
        TransportPlan otherTransportPlan =  transportPlanList.get(1);
        delayParamObject = new DelayParamObject(10, firstSection.getStartMilestone().getId());

        addDelayExpectBadRequest(otherTransportPlan.getId(), delayParamObject, USER, PWD);
    }

    @Test
    public void addDelayExpectNotFound() {
        initDbService.insertTestData();
        TransportPlan transportPlan = getTransportPlan();
        DelayParamObject delayParamObject = new DelayParamObject(10, 666);

        addDelayExpectNotFound(transportPlan.getId(), delayParamObject, USER, PWD);

        Section firstSection = transportPlan.getSections().get(0);
        delayParamObject = new DelayParamObject(10, firstSection.getStartMilestone().getId());

        addDelayExpectNotFound(666, delayParamObject, USER, PWD);
    }

    @Test
    public void addDelayExpectForbidden() {
        initDbService.insertTestData();
        TransportPlan transportPlan = getTransportPlan();
        DelayParamObject delayParamObject = new DelayParamObject(10, 666);

        addDelayExpectNotFound(transportPlan.getId(), delayParamObject, USER, PWD);

        Section firstSection = transportPlan.getSections().get(0);
        delayParamObject = new DelayParamObject(10, firstSection.getStartMilestone().getId());

        addDelayExpectForbidden(666, delayParamObject, "test", "test");
    }


    @Test
    public void delayToStartMilestoneTest() throws TransportPlanNotFoundException {
        initDbService.insertTestData();
        TransportPlan transportPlan = getTransportPlan();

        decreaseConfig.getDecreaseCategories();

        for (DecreaseCategory decreaseCategory : decreaseConfig.getDecreaseCategories()) {
            if (decreaseCategory.getDelayLimit() != 0) {
                testDelayToTransportPlan(decreaseCategory.getDelayLimit() - 10, true);
            }
            testDelayToTransportPlan(decreaseCategory.getDelayLimit() + 10,  true);
        }
    }


    @Test
    public void delayToEndMilestoneTest() throws TransportPlanNotFoundException {
        decreaseConfig.getDecreaseCategories();

        for (DecreaseCategory decreaseCategory : decreaseConfig.getDecreaseCategories()) {
            if (decreaseCategory.getDelayLimit() != 0) {
                testDelayToTransportPlan(decreaseCategory.getDelayLimit() - 10, false);
            }
            testDelayToTransportPlan(decreaseCategory.getDelayLimit() + 10, false);
        }
    }


    private void testDelayToTransportPlan(int delay, boolean startMilestone) throws TransportPlanNotFoundException {
        initDbService.clearDB();
        initDbService.insertTestData();
        TransportPlan transportPlan = getTransportPlan();
        DecreaseCategory decreaseCategory = DecreaseCategoryUtil.selectCategory(delay, decreaseConfig.getDecreaseCategories());

        double expectedIncome = transportPlan.getExpectedIncome();

        Section firstSection = transportPlan.getSections().get(0);
        Section secondSection = transportPlan.getSections().get(1);
        Section thirdSection = transportPlan.getSections().get(2);
        Section lastSection = transportPlan.getSections().get(transportPlan.getSections().size()-1);

        LocalDateTime startTimeInFirstSection = firstSection.getStartMilestone().getPlannedTime();
        LocalDateTime endTimeInFirstSection = firstSection.getEndMilestone().getPlannedTime();

        LocalDateTime startTimeInSecondSection = secondSection.getStartMilestone().getPlannedTime();
        LocalDateTime endTimeInSecondSection = secondSection.getEndMilestone().getPlannedTime();

        LocalDateTime startTimeInThirdSection = thirdSection.getStartMilestone().getPlannedTime();
        LocalDateTime endTimeInThirdSection = thirdSection.getEndMilestone().getPlannedTime();

        LocalDateTime startTimeInLastSection = lastSection.getStartMilestone().getPlannedTime();
        LocalDateTime endTimeInLastSection = lastSection.getEndMilestone().getPlannedTime();

        DelayParamObject delayParamObject;
        if (startMilestone) {
            delayParamObject = new DelayParamObject(delay, secondSection.getStartMilestone().getId());
        } else {
            delayParamObject = new DelayParamObject(delay, secondSection.getEndMilestone().getId());
        }

        addDelayToTransportPlan(transportPlan.getId(), delayParamObject);

        transportPlan =  transportPlanService.findById(transportPlan.getId());

        assertThat(transportPlan.getExpectedIncome()).isEqualTo(expectedIncome * ( 1 - ( (double)decreaseCategory.getDecreasePercent())/100));

        firstSection = transportPlan.getSections().get(0);
        secondSection = transportPlan.getSections().get(1);
        thirdSection = transportPlan.getSections().get(2);
        lastSection = transportPlan.getSections().get(transportPlan.getSections().size()-1);

        assertThat(startTimeInFirstSection.withNano(0)).isEqualTo(firstSection.getStartMilestone().getPlannedTime().withNano(0));
        assertThat(endTimeInFirstSection.withNano(0)).isEqualTo(firstSection.getEndMilestone().getPlannedTime().withNano(0));

        if (startMilestone) {
            assertThat(startTimeInSecondSection.plusMinutes(delay).withNano(0)).isEqualTo(secondSection.getStartMilestone().getPlannedTime().withNano(0));
            assertThat(startTimeInThirdSection.withNano(0)).isEqualTo(thirdSection.getStartMilestone().getPlannedTime().withNano(0));
            assertThat(endTimeInThirdSection.withNano(0)).isEqualTo(thirdSection.getEndMilestone().getPlannedTime().withNano(0));
        } else {
            assertThat(startTimeInSecondSection.withNano(0)).isEqualTo(secondSection.getStartMilestone().getPlannedTime().withNano(0));
            assertThat(startTimeInThirdSection.plusMinutes(delay).withNano(0)).isEqualTo(thirdSection.getStartMilestone().getPlannedTime().withNano(0));
            assertThat(endTimeInThirdSection.withNano(0)).isEqualTo(thirdSection.getEndMilestone().getPlannedTime().withNano(0));
        }

        assertThat(endTimeInSecondSection.plusMinutes(delay).withNano(0)).isEqualTo(secondSection.getEndMilestone().getPlannedTime().withNano(0));

        assertThat(startTimeInLastSection.withNano(0)).isEqualTo(lastSection.getStartMilestone().getPlannedTime().withNano(0));
        assertThat(endTimeInLastSection.withNano(0)).isEqualTo(lastSection.getEndMilestone().getPlannedTime().withNano(0));
    }


    public TransportPlan getTransportPlan() {
        List<TransportPlan> transportPlanList = transportPlanRepository.findAll();
        return transportPlanList.stream().findFirst().get();
    }


    private TransportPlanDto addDelayToTransportPlan(long transportPlanId, DelayParamObject delayParamObject) {
        return callRequest(webTestClient.post().uri(API_TRANSPORT_PLANS+"/"+transportPlanId+"/delay"), delayParamObject, "TransportManager", "test");
    }


    private TransportPlanDto callRequest(WebTestClient.RequestBodySpec requestBodySpec, DelayParamObject delayParamObject, String username, String password) {
        return requestBodySpec.bodyValue(delayParamObject)
                .headers(headers -> headers.setBasicAuth(username, password))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(TransportPlanDto.class)
                .returnResult()
                .getResponseBody();
    }

    public void addDelayExpectBadRequest(long transportPlanId, DelayParamObject delayParamObject, String username, String password) {
        webTestClient.post()
                .uri(API_TRANSPORT_PLANS + "/" +transportPlanId+"/delay")
                .headers(headers -> headers.setBasicAuth(username, password))
                .bodyValue(delayParamObject)
                .exchange()
                .expectStatus().isBadRequest();
    }

    public void addDelayExpectNotFound(long transportPlanId, DelayParamObject delayParamObject, String username, String password) {
        webTestClient.post()
                .uri(API_TRANSPORT_PLANS + "/" +transportPlanId+"/delay")
                .headers(headers -> headers.setBasicAuth(username, password))
                .bodyValue(delayParamObject)
                .exchange()
                .expectStatus().isNotFound();
    }

    public void addDelayExpectForbidden(long transportPlanId, DelayParamObject delayParamObject, String username, String password) {
        webTestClient.post()
                .uri(API_TRANSPORT_PLANS + "/" +transportPlanId+"/delay")
                .headers(headers -> headers.setBasicAuth(username, password))
                .bodyValue(delayParamObject)
                .exchange()
                .expectStatus().isForbidden();
    }

}
