package hu.cubix.sco.logistics.controller;

import hu.cubix.sco.logistics.dto.AddressDto;
import hu.cubix.sco.logistics.model.Address;
import hu.cubix.sco.logistics.service.AddressService;
import hu.cubix.sco.logistics.service.InitDbService;
import hu.cubix.sco.logistics.service.search.AddressSearchObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase
public class AddressIntegrationTest {

    @Autowired
    InitDbService initDbService;

    @Autowired
    WebTestClient webTestClient;

    @Autowired
    AddressService addressService;

    public static final String API_ADDRESS = "/api/addresses";

    @BeforeEach
    public  void beforeMethod() {
        initDbService.clearDB();
    }

    @Test
    public void addAddressTest() {
        AddressDto addressDto = new AddressDto("HU", "Budapest", "Chasar Andras", "1146", "2", 47.5050016, 19.0919339);
        int beforeSize = addressService.findAll().size();

        addAddress(addressDto);
        List<Address> addedAddresses = addressService.findAll();
        int afterSize = addedAddresses.size();
        Address addedAddress = addedAddresses.get(0);

        assertThat(beforeSize).isEqualTo(afterSize-1);

        compareAddressObjcts(addedAddress, addressDto);
    }

    @Test
    public void deleteAddressTest() {
        AddressDto addressDto = new AddressDto("HU", "Budapest", "Chasar Andras", "1146", "2", 47.5050016, 19.0919339);
        int beforeSize = addressService.findAll().size();

        addAddress(addressDto);

        List<Address> addedAddresses = addressService.findAll();
        int afterSize = addedAddresses.size();
        Address addedAddress = addedAddresses.get(0);

        assertThat(beforeSize).isEqualTo(afterSize-1);

        delete(addedAddress.getId(),"AddressManager", "test");

        afterSize = addressService.findAll().size();
        assertThat(beforeSize).isEqualTo(afterSize);
    }


    @Test
    public void updateAddressTest() {
        AddressDto addressDto = new AddressDto("HU", "Budapest", "Chasar Andras", "1146", "2", 47.5050016, 19.0919339);

        addAddress(addressDto);

        List<Address> addedList = addressService.findAll();
        int beforeUpdateSize = addedList.size();
        Address addedAddress = addedList.get(0);
        assertThat(beforeUpdateSize).isEqualTo(1);

        addressDto = new AddressDto("UK",
                "London",
                "St Albert",
                "11 11",
                "22",
                47.5555555,
                19.9999999);

        updateAddress(addedAddress.getId(), addressDto);

        addedList = addressService.findAll();
        int afterUpdateSize = addedList.size();
        addedAddress = addedList.get(0);
        assertThat(beforeUpdateSize).isEqualTo(1);

        compareAddressObjcts(addedAddress, addressDto);
    }

    @Test
    public void searchAddressTest() {
        AddressDto addressDto = new AddressDto(
                "HU",
                "Budapest",
                "Chasar Andras",
                "1146",
                "2",
                47.5050016,
                19.0919339);

        addAddress(addressDto);

        addressDto = new AddressDto(
                "UK",
                "London",
                "St Albert",
                "11 11",
                "22",
                47.5555555,
                19.9999999);

        addAddress(addressDto);

        addressDto = new AddressDto(
                "UK",
                "London",
                "St Louise",
                "22 22",
                "33",
                50.5555555,
                20.9999999);

        addAddress(addressDto);

        AddressSearchObject searchObject = new AddressSearchObject();

        Pageable pageable = PageRequest.of(1, 10);
        Page<Address> result = addressService.search(searchObject, pageable);

        assertThat(result.getTotalElements()).isEqualTo(3);

        searchObject = new AddressSearchObject();
        searchObject.setCity("London");

        result = addressService.search(searchObject, pageable);

        assertThat(result.getTotalElements()).isEqualTo(2);

        searchObject = new AddressSearchObject();
        searchObject.setCountryISOCode("UK");

        result = addressService.search(searchObject, pageable);

        assertThat(result.getTotalElements()).isEqualTo(2);

        searchObject = new AddressSearchObject();
        searchObject.setZipCode("11 11");

        result = addressService.search(searchObject, pageable);

        assertThat(result.getTotalElements()).isEqualTo(1);

        searchObject = new AddressSearchObject();
        searchObject.setStreet("St");

        result = addressService.search(searchObject, pageable);

        assertThat(result.getTotalElements()).isEqualTo(2);

        searchObject = new AddressSearchObject();
        searchObject.setStreet("st ");

        result = addressService.search(searchObject, pageable);

        assertThat(result.getTotalElements()).isEqualTo(2);

        searchObject = new AddressSearchObject();
        searchObject.setZipCode("333");

        result = addressService.search(searchObject, pageable);

        assertThat(result.getTotalElements()).isEqualTo(0);

        searchObject = new AddressSearchObject();
        searchObject.setCity("lon");

        result = addressService.search(searchObject, pageable);

        assertThat(result.getTotalElements()).isEqualTo(2);


        searchObject = new AddressSearchObject();
        searchObject.setCountryISOCode("SK");

        result = addressService.search(searchObject, pageable);

        assertThat(result.getTotalElements()).isEqualTo(0);

        searchObject = new AddressSearchObject();
        searchObject.setCountryISOCode("UK");
        searchObject.setCity("London");
        searchObject.setStreet("st");
        searchObject.setZipCode("11 11");

        result = addressService.search(searchObject, pageable);

        assertThat(result.getTotalElements()).isEqualTo(1);
    }


    private void compareAddressObjcts(Address address, AddressDto addressDto) {
        assertThat(addressDto.getHouseNumber()).isEqualTo(address.getHouseNumber());
        assertThat(addressDto.getCity()).isEqualTo(address.getCity());
        assertThat(addressDto.getCountryISOCode()).isEqualTo(address.getCountryISOCode());
        assertThat(addressDto.getLatitude()).isEqualTo(address.getLatitude());
        assertThat(addressDto.getLongitude()).isEqualTo(address.getLongitude());
        assertThat(addressDto.getZipCode()).isEqualTo(address.getZipCode());
        assertThat(addressDto.getStreet()).isEqualTo(address.getStreet());
    }


    private AddressDto addAddress(AddressDto addressDto) {
        return callRequest(webTestClient.post().uri(API_ADDRESS), addressDto,"AddressManager", "test");
    }

    private AddressDto updateAddress(long id, AddressDto addressDto) {
        return callRequest(webTestClient.put().uri(API_ADDRESS+"/"+id), addressDto, "AddressManager", "test");
    }

    public void delete(long id, String username, String password) {
        webTestClient
                .delete()
                .uri(API_ADDRESS+"/"+id)
                .headers(headers -> headers.setBasicAuth(username, password))
                .exchange()
                .expectStatus()
                .isOk();
    }

    private AddressDto callRequest(
            WebTestClient.RequestBodySpec requestBodySpec,
            AddressDto addressDto,
            String username,
            String password) {

        return requestBodySpec.bodyValue(addressDto)
                .headers(headers -> headers.setBasicAuth(username, password))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(AddressDto.class)
                .returnResult()
                .getResponseBody();
    }


}
